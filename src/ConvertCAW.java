import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.time.LocalTime;

public class ConvertCAW {

    private static JFrame frame;
    private static JPanel panel;
    private static JButton button;
    private static LocalDate now;
    private static String time;
    private static JTextField text;
    private static JLabel labeltext;
    private static JLabel labelresult;
    private static JLabel labeltime;
    private static long startTime;
    private static String date;
    private static String date2;
    private static int recordCount;
    private static Properties prop;
    private static boolean statusEmail;
    private static DateTimeFormatter dtf;
    public static void gui(){

        //deklarasi private frame
        frame = new JFrame("Converter CAW-DCC");
        frame.setVisible(true);
        frame.setSize(500,250);
        frame.setMaximumSize(new Dimension(500,250));
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        //deklarasi private panel
        panel = new JPanel();
        panel.setBackground(Color.orange);

        //deklarasi private label
        labeltext = new JLabel("<html><center>Converter for CAW Auto Debet to DCC<br>"+
                "Version 30 June 2020<br><br>" +
                "Please put the CAW file into input folder, the result will be generated in output folder<br><br>" +
                "Instruction Date (optional)(YYYY-MM-DD)</center></html>");
        labeltext.setBackground(Color.orange);
//        Border border= LineBorder.createBlackLineBorder();
//        labeltext.setBorder(border);

        labelresult = new JLabel("");
        labelresult.setBackground(Color.orange);
        labeltime = new JLabel("");
        labeltime.setBackground(Color.orange);

        //deklarasi private button
        button = new JButton("Convert");

        JDateChooser chooser = new JDateChooser();
        chooser.setDateFormatString("yyyy-MM-dd");
//        chooser.getDate();
        //memanggil fungsi deklarasi
        panel.add(labeltext);
        panel.add(chooser);
        panel.add(button);

        //memanggil deklarasi button+button aksi
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startTime = System.nanoTime();
                System.out.println(chooser.getDate());
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                date="";
                if (chooser.getDate()!=null) {
                    date = format1.format(chooser.getDate());

                    Calendar c = Calendar.getInstance();
                    try {
                        c.setTime(format1.parse(format1.format(chooser.getDate())));
                    } catch (ParseException parseException) {
//                    parseException.printStackTrace();
                    }
                    c.add(Calendar.DATE, 1);
                    date2 = format1.format(c.getTime());
                    ;
                    System.out.println(date);
                    System.out.println(date2);

                }

                String tempNow = now.toString();
                int compare= date.compareTo(tempNow);



                //untuk membandingnkan date
                if (!date.equals("")) {
                    if (compare>0) {
                        System.out.println("");
                        ReadFileCAW();
                        labelresult.setText("");
                        long endTime   = System.nanoTime();
                        long totalTime = endTime - startTime;
                        float sec= totalTime/1000000000;
                        int seconds= (int)sec;
                        if(statusEmail) {
                            labeltime.setText("Successfully Converting " + recordCount + " CAW record(s) To DCC File in " + seconds + " seconds");
                            panel.add(labeltime);
                        }else{
                            labeltime.setText("Email couldn't be more than 1 (Set In Txt File)");
                            panel.add(labeltime);
                        }
                    }else if(compare==0){
                        int timeCompare= prop.getProperty("app.instruction_at").compareTo("070000");
                        if (timeCompare<0){
                            System.out.println("");
                            ReadFileCAW();
                            labelresult.setText("");
                            long endTime   = System.nanoTime();
                            long totalTime = endTime - startTime;
                            float sec= totalTime/1000000000;
                            int seconds= (int)sec;
                            if(statusEmail) {
                                labeltime.setText("Successfully Converting" + recordCount + " CAW record(s) To DCC File in " + seconds + " seconds");
                                panel.add(labeltime);
                            }else{
                                labeltime.setText("Email couldn't be more than 1 (Set In Txt File)");
                                panel.add(labeltime);
                            }
                        }else{
                            System.out.println("Instruction At (set in configuration file) has been passed");
                            labeltime.setText("Instruction At (set in configuration file) has been passed");
                            panel.add(labeltime);
                        }
                    } else {
                        System.out.println("Instruction Date Must Be Empty Or Equal Or Greater Than Today");
                        labelresult.setText("Instruction Date Must Be Empty Or Equal Or Greater Than Today");
                    }
                }else{
                    System.out.println("");
                    ReadFileCAW();
                    labelresult.setText("");
                    long endTime   = System.nanoTime();
                    long totalTime = endTime - startTime;
                    float sec= totalTime/1000000000;
                    int seconds= (int)sec;
                    if(statusEmail) {
                        labeltime.setText("Successfully Converting " + recordCount + " CAW record(s) To DCC File in " + seconds + " seconds");
                        panel.add(labeltime);
                    }else{
                        labeltime.setText("Email couldn't be more than 1");
                        panel.add(labeltime);
                    }
                }
            }
        });

        panel.add(labelresult);
        panel.add(labeltime);
        frame.add(panel);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        prop = new Properties();
        String properties = "app.config";
        InputStream config = null;
        try {
            config = new FileInputStream(properties);
            prop.load(config);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        startTime = System.nanoTime();
        dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        now = LocalDate.now();

        time = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
        gui();
    }

    //untuk read file caw
    public static void ReadFileCAW(){
        recordCount=0;
        //membaca semua file di folder IN
        System.out.println(prop.getProperty("app.folder_IN"));
        File dir= new File(prop.getProperty("app.folder_IN"));
        String[] fileList= dir.list();
        for (String name:fileList) {
            try {
                FileReader fileReader = new FileReader(prop.getProperty("app.folder_IN")+name);
                CSVReader csvReader = new CSVReader(fileReader);
                //membaca semua folder in
                List<String[]> records = csvReader.readAll();
                //menghasilkan output di folder OUT sesuai nama folder IN
                String[] out= name.split("\\.");
                File file = new File(prop.getProperty("app.folder_OUT")+out[0]+"_converted_to_DCC.csv");
                //menghasilkan output di folder OUT sesuai nama folder IN
                FileWriter outputfile = new FileWriter(file);
                CSVWriter writer = new CSVWriter(outputfile, ',', '\u0000', ' ', "\n");

                //membuat header

                Properties prop = new Properties();
                String properties = "app.config";
                InputStream config = new FileInputStream(properties);
                prop.load(config);

                if (!date.equals("")) {
                    writer.writeNext(new String[]{"H", prop.getProperty("app.credit_account_no"), prop.getProperty("app.credit_account_description")
                            , "S", date.replace("-", ""), prop.getProperty("app.instruction_at"), date2.replace("-","")});
                } else {
                    writer.writeNext(new String[]{"H", prop.getProperty("app.credit_account_no"), prop.getProperty("app.credit_account_description")
                            , "S", "", "", ""});
                }

                //membuat detail
                for (String[] record : records) {
                    int length= record.length-1;
                    System.out.println(length);
                    recordCount += 1;
                    if(length==3) {
                        writer.writeNext(new String[]{"D", "", "", record[3], record[0], "" +
                                "", prop.getProperty("app.debit_account_currency_code"), record[1], "","REM",
                                "", record[2], "", "", "", "", "", "", "", "", "",});
                        statusEmail=true;
                    }
                    if(length==4){
                        writer.writeNext(new String[]{"D", "", "", record[3], record[0], "" +
                                "", prop.getProperty("app.debit_account_currency_code"), record[1],"","REM",
                                "", record[2], record[4], "", "", "", "", "", "", "",});
                        statusEmail=true;
                    }
                    if(length>4){
                        statusEmail=false;
                        return;
                    }
//                    if(length==5){
//                        writer.writeNext(new String[]{"D", "", "", record[3], record[0], "" +
//                                "", prop.getProperty("app.debit_account_currency_code"), record[1],"","REM",
//                                "", record[2], record[4]+";"+record[5], "", "", "", "", "", "", "",});
//                    }
//                    if(length==6){
//                        writer.writeNext(new String[]{"D", "", "", record[3], record[0], "" +
//                                "", prop.getProperty("app.debit_account_currency_code"), record[1],"","REM",
//                                "", record[2], record[4]+";"+record[5]+";"+record[6], "", "", "", "", "", "", "",});
//                    }


                }
                writer.close();

            } catch (IOException | CsvException e) {
                e.printStackTrace();
            }
        }
    }
};

